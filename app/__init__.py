import os
from flask import Flask
from mongoengine import connect
from .util import JSONEncoder


if __name__ == 'app':
    connect('rocket_api', host=os.getenv('MONGO_HOST'))
    app = Flask(__name__)

    app.json_encoder = JSONEncoder

    from app.controller import *
