import json
from flask import jsonify, request
from bson.objectid import ObjectId
from bson.errors import InvalidId

from app import app
from app.schema import sector_schema, update_sector_schema
from app.model import Sector
from app.util import validate_input


# Sector
@app.route('/sector', methods=['GET'])
def list_sectors(page=0, per_page=10):
    try:
        page = int(request.args.get('page', page))
        per_page = int(request.args.get('per_page', per_page))
    except ValueError as err:
        message = 'Validation error: Invalid parameter for page/per_page.' + str(err)
        app.logger.error(message)
        return jsonify({'error': message}), 422

    data = Sector.objects()
    return {
        'sectors': [json.loads(o.to_json()) for o in data[page * per_page:(page + 1) * per_page]],
        'total_found': data.count(),
        'page': page,
        'per_page': per_page
    }


@app.route('/sector/<sector_id>', methods=['GET'])
def get_sector(sector_id):
    try:
        ObjectId(sector_id)
    except InvalidId as err:
        return jsonify({'error': 'Invalid Id. ' + str(err)}), 422
    data = Sector.objects(id=sector_id)
    if data.count():
        return jsonify({
            'sector': json.loads(data[0].to_json())
        })
    else:
        message = 'Sector not found.'
        app.logger.error(message)
        return jsonify({'error': message}), 404


@app.route('/sector', methods=['POST'])
def add_sector():
    app.logger.info(request.data)
    input_data = validate_input(sector_schema, request.data)
    if isinstance(input_data, tuple):
        return input_data

    existing_sector = Sector.objects(name=input_data['name'])
    if existing_sector.count():
            message = 'Already exists this sector with the given name.'
            app.logger.error(message)
            return jsonify({'error': message}), 409

    sector = Sector(**input_data)
    sector.save()
    if sector.id:
        return jsonify({
            'sector': json.loads(sector.to_json())
        }), 201
    else:
        app.logger.error('Failed to insert data to collection.')
        return jsonify({'error': 'Internal server error. Failed to insert data to collection.'}), 500


@app.route('/sector', methods=['PATCH'])
def update_sector():
    input_data = validate_input(update_sector_schema, request.data)
    if isinstance(input_data, tuple):
        return input_data

    sector_id = input_data.pop('id')
    try:
        ObjectId(sector_id)
    except InvalidId as err:
        message = 'Invalid Id. ' + str(err)
        app.logger.error(message)
        return jsonify({'error': message}), 422

    data = Sector.objects(id=sector_id)
    if data.count():
        data[0].update(**input_data)
        data = Sector.objects(id=sector_id)[0]
        return jsonify({
            'sector': json.loads(data .to_json())
        })
    else:
        message = 'Sector not found.'
        app.logger.error(message)
        return jsonify({'error': message}), 404


@app.route('/sector/<sector_id>', methods=['DELETE'])
def delete_sector(sector_id):
    try:
        ObjectId(sector_id)
    except InvalidId as err:
        message = 'Invalid Id. ' + str(err)
        app.logger.error(message)
        return jsonify({'error': message}), 422

    data = Sector.objects(id=sector_id)
    if data.count():
        deleted = data[0]
        data[0].delete()
        return jsonify({
            'sector': json.loads(deleted.to_json())
        })
    else:
        message = 'Sector not found.'
        app.logger.error(message)
        return jsonify({'error': message}), 404
