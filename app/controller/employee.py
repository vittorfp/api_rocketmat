import json
from datetime import datetime
from flask import jsonify, request
from bson.objectid import ObjectId
from bson.errors import InvalidId
from app import app
from app.schema import employee_schema, update_employee_schema
from app.model import Employee, Sector
from app.util import validate_input


# Employee
@app.route('/employee', methods=['GET'])
def list_employees(page=0, per_page=10):
    try:
        page = int(request.args.get('page', page))
        per_page = int(request.args.get('per_page', per_page))
    except ValueError as err:
        message = 'Validation error: Invalid parameter for page/per_page.' + str(err)
        app.logger.error(message)
        return jsonify({'error': message}), 422

    data = Employee.objects()
    return {
        'employees': [json.loads(o.to_json()) for o in data[page * per_page:(page + 1) * per_page]],
        'total_found': data.count(),
        'page': page,
        'per_page': per_page
    }


@app.route('/employee/<employee_id>', methods=['GET'])
def get_employee(employee_id):
    try:
        ObjectId(employee_id)
    except InvalidId as err:
        return jsonify({'error': 'Invalid Id. ' + str(err)}), 422
    data = Employee.objects(id=employee_id)
    if data.count():
        return jsonify({
            'employee': json.loads(data[0].to_json())
        })
    else:
        message = 'Employee not found.'
        app.logger.error(message)
        return jsonify({'error': message}), 404


@app.route('/employee', methods=['POST'])
def add_employee():
    app.logger.info(request.data)
    input_data = validate_input(employee_schema, request.data)
    if isinstance(input_data, tuple):
        return input_data

    existing_employees = Employee.objects(name=input_data['name'])
    if existing_employees.count():
            message = 'Already exists an Employee with the given name.'
            app.logger.error(message)
            return jsonify({'error': message}), 409

    if 'sector' in input_data:
        try:
            ObjectId(input_data['sector']['id'])
        except InvalidId as err:
            return jsonify({'error': 'Invalid Id. ' + str(err)}), 422

        if not Sector.objects(id=input_data['sector']['id']).count():
            return jsonify({'error': 'Sector not found in database.'}), 404

    input_data.update({
        'registrationDate': datetime.utcnow()
    })
    employee = Employee(**input_data)
    employee.save()
    if employee.id:
        return jsonify({
            'employee': json.loads(employee.to_json())
        }), 201
    else:
        app.logger.error('Failed to insert data to collection.')
        return jsonify({'error': 'Internal server error. Failed to insert data to collection.'}), 500


@app.route('/employee', methods=['PATCH'])
def update_employee():
    input_data = validate_input(update_employee_schema, request.data)
    if isinstance(input_data, tuple):
        return input_data

    employee_id = input_data.pop('id')
    try:
        ObjectId(employee_id)
    except InvalidId as err:
        message = 'Invalid Id. ' + str(err)
        app.logger.error(message)
        return jsonify({'error': message}), 422

    data = Employee.objects(id=employee_id)
    if data.count():
        input_data.update({
            'lastUpdateDate': datetime.utcnow()
        })
        data[0].update(**input_data)
        data = Employee.objects(id=employee_id)[0]
        return jsonify({
            'employee': json.loads(data .to_json())
        })
    else:
        message = 'Employee not found.'
        app.logger.error(message)
        return jsonify({'error': message}), 404


@app.route('/employee/<employee_id>', methods=['DELETE'])
def delete_employee(employee_id):
    try:
        ObjectId(employee_id)
    except InvalidId as err:
        message = 'Invalid Id. ' + str(err)
        app.logger.error(message)
        return jsonify({'error': message}), 422

    data = Employee.objects(id=employee_id)
    if data.count():
        deleted = data[0]
        data[0].delete()
        return jsonify({
            'employee': json.loads(deleted.to_json())
        })
    else:
        message = 'Employee not found.'
        app.logger.error(message)
        return jsonify({'error': message}), 404
