from .employee import employee_schema, update_employee_schema
from .sector import sector_schema, update_sector_schema
