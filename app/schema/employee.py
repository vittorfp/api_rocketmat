from marshmallow import Schema, fields
from .sector import UpdateSector


class Employee(Schema):
    class Meta:
        unknown = 'raise'

    name = fields.Str(required=True)
    birthDate = fields.Date(required=True)
    wage = fields.Number(required=True)
    active = fields.Bool(required=False, default=True)
    sector = fields.Nested(UpdateSector)


class UpdateEmployee(Schema):
    class Meta:
        unknown = 'raise'

    name = fields.Str(required=False)
    birthDate = fields.Date(required=False)
    wage = fields.Number(required=False)
    active = fields.Bool(required=False, default=True)
    sector = fields.Nested(UpdateSector)
    id = fields.Str(required=True)


employee_schema = Employee()
update_employee_schema = UpdateEmployee()
