from marshmallow import Schema, fields


class Sector(Schema):
    class Meta:
        unknown = 'raise'

    name = fields.Str(required=True)


class UpdateSector(Schema):
    class Meta:
        unknown = 'raise'

    name = fields.Str(required=False)
    id = fields.Str(required=True)


sector_schema = Sector()
update_sector_schema = UpdateSector()
