from datetime import datetime
from mongoengine import StringField, DateTimeField, DecimalField, BooleanField, ReferenceField
import mongoengine_goodjson as gj
from .sector import Sector


class Employee(gj.Document):
    name = StringField(required=True, max_length=80)
    birthDate = DateTimeField(required=True)
    wage = DecimalField(required=True)
    active = BooleanField(required=True, default=True)
    sector = ReferenceField(Sector)
    registrationDate = DateTimeField(required=False)
    lastUpdateDate = DateTimeField(required=True, default=datetime.utcnow())
