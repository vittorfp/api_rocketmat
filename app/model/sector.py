from mongoengine import StringField
import mongoengine_goodjson as gj


class Sector(gj.Document):
    name = StringField(max_length=200, unique=True)
