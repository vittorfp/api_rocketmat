#!flask/bin/python
import os
from app import app

if __name__ == '__main__':
    app.run(
        host=os.getenv('HOST'),
        port=5000,
        debug=True
    )
