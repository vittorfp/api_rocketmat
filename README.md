# Rocketmat API

This code exposes routes to CRUD the Employee and Sector entities. 

**Local deploy**

To build the container just run the following command on the root folder of the repo:

````
docker-compose up -d
# or
docker-compose up -d --build
# to force the container rebuild
````

To see the logs use the following:

````
docker-compose logs -f
````


**Postman collection**

There is a postman collection file that exemplifies the route calls. Import the `Rocketmat API.postman_collection.json` on your postman to see it.

**Routes documentation**

Due to time issues the Swagger docs were not created, however, here is some explanation about the routes:

* GET /employee and /sector

Get a list of employees/sectors. Use de query strings parameters *page* and *per_page* to paginate and select the number of results per page.

Example: /employee?page=1&per_page=10
The default *page* is 0 and the default *per_page* is 10.


* GET /employee/<employee_id> and /sector/<sector_id>

Get an specific employee/sector given the *id*.
Example: /sector/5d39ae812de38795e8e97d39


* POST /employee and /sector

Insert an employee/sector in the database. 

1. Example:

POST /employee
body: 
```json
{
	"name": "José Lino 3",
	"birthDate": "1996-01-06T02:00:00.000Z",
	"wage": 100.0,
	"sector": {
        "id": "5d39ae812de38795e8e97d39"
    }
}
```

The fields *name*, *birthDate* and *wage* are required.


2. POST /sector
body: 
```json
{
	"name": "Setor de compras"
}
```
The field *name* is required.

* PATH /employee and /sector

Do a partial update in a given object.
Example:
PATCH /employee
```json
{
    "id": "5d39ae812de38795e8e97d39",
    "active": false
}
```
Only the field *id* is required. 


* DELETE /employee/<employee_id> and /sector/<sector_id>

Deletes an object. Same signature as the equivalent GET methods.

**TODO**

1. In a production environment there would be an specific docker-compose file pointing to the correct database, maybe an AWS DocumentDB instance. This would suit the horizontal auto scaling the microservice.
2. Automated tests using pytest.
3. Include Swagger.
4. Implement logical deletion in the Employee entity (if active is false the document is logically deleted). 
5. Setup automatic build and store the container in ECR.
6. Setup ECS container pointing to the previously cited ECR container.
